# RNA-Seq from raw reads to counts

## Pre-requisities
* [Linux](https://sps-bioinfo.pages.mia.inra.fr/genome-annotation-workshop/pages/linux.html)
* [Cluster](http://genoweb.toulouse.inra.fr/~formation/cluster/doc/Formation_cluster_SLURM.pdf)


## Data source

* genome (JN3_SC00.fasta)
* raw reads (2022-242_S6.lepto.SC00.R1.fastq.gz, 2022-242_S6.lepto.SC00.R2.fastq.gz)
* annotations (JN3_SC00.transcripts.gff)

Look at fasta/gff files.

```
# get genome (JN3_SC00)
wget https://forgemia.inra.fr/bioger/ateliers_bioinfo/-/raw/main/data/JN3_SC00.fasta
# get R1
wget https://forgemia.inra.fr/bioger/ateliers_bioinfo/-/raw/main/data/2022-242_S6.lepto.SC00.R1.fastq.gz
# get R2
wget https://forgemia.inra.fr/bioger/ateliers_bioinfo/-/raw/main/data/2022-242_S6.lepto.SC00.R2.fastq.gz
# get annotations
wget https://forgemia.inra.fr/bioger/ateliers_bioinfo/-/raw/main/data/JN3_SC00.transcripts.gff
```

## Data Quality

We start with quality control of fastq files.

Fastq format [specifications](https://fr.wikipedia.org/wiki/FASTQ):
```
@VH00567:1:AAAM2YLHV:1:1101:9862:45451/1
CGGGTATGACCGCTACAGTGCCCACATAGAGGAAGGTGCCTGCTGTGAAGGGGAGCAACATGTCTCCCCATTGCAAACTCGTTCCAAACAGCCGAGGCCCATGAGATCC
+
CCCCCC-CC;-CCCCCC;-;-CCCCC;CCCC;CCCC;CC;CCC;CCC-CCCCCC;CCCCC-CCCCCC;C;C;CCCCCCCC--C-CC;;CCCCC--;C;;;C-C-CCC-;
@VH00567:1:AAAM2YLHV:1:1101:9956:28205/1
CTCGGAACAAAACCACCGTTCAAACCCTGCTTTAGGCATAGGCCTGTGCCTGTGGATGGCTTTTGACAGCCATACGTAGCCACGACATAAAACTGTCTCTTATACACAT
+
CCCCCCCCCCCCCCCCCCCCCCC-CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC;
...
```
if paired, read 2 with /2
```
@VH00567:1:AAAM2YLHV:1:1101:9862:45451/2
CTCCCAGTCTGCGGGATCACATGGGCCAGGGCTGTTTGGAACGAGTTTGCAATGGGGAGACATGTTGCTCCCCTTCACAGCAGGCACCTTCCTCTATGTGGGCACTGTA
+
;;C-C;C;C;CCC;CCCC;C;---C-CCCC;CCCC-C-;CC--C;-;;CCCC;;CCCCC-CCCCC-CCCCCCC-CCCCC;C-CC;C;CCCC-;CCCCCCC;;CC;CCC-
@VH00567:1:AAAM2YLHV:1:1101:9956:28205/2
TTTATGTCGTGGCTACGTATGGCTGTCAAAAGCCATCCACAGGCACAGGCCTATGCCTAAAGCAGGGTTTGAACGGTGGTTTTGTTCCGAGACTGTCTCTTATACACAT
+
CCCCCCCCCCCCCCCCCCC;CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC;CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
...
```

Run Fastqc on reads:

link to a [fastqc example](https://rtsf.natsci.msu.edu/genomics/tech-notes/fastqc-tutorial-and-faq/)

genologin:
```
module load bioinfo/FastQC_v0.11.7
fastqc 2022-242_S6.lepto.SC00.R1.fastq.gz 2022-242_S6.lepto.SC00.R2.fastq.gz
```

## cleaning/trimming data

command:
```
trimmomatic PE -threads {threads} -phred33 {input.R1} {input.R2} {output.R1_paired} {output.R1_unpaired} {output.R2_paired} {output.R2_unpaired} ILLUMINACLIP:{params.adapters}:2:30:12:1:true SLIDINGWINDOW:4:20 MINLEN:16
```

genologin:
```
module load bioinfo/Trimmomatic-0.39
java -jar /usr/local/bioinfo/src/Trimmomatic/Trimmomatic-0.39/trimmomatic.jar PE -phred33 2022-242_S6.lepto.SC00.R1.fastq.gz 2022-242_S6.lepto.SC00.R2.fastq.gz 2022-242_S6.lepto.SC00.R1.paired.fastq.gz 2022-242_S6.lepto.SC00.R1.unpaired.fastq.gz 2022-242_S6.lepto.SC00.R2.paired.fastq.gz 2022-242_S6.lepto.SC00.R2.unpaired.fastq.gz ILLUMINACLIP:/usr/local/bioinfo/src/Trimmomatic/Trimmomatic-0.39/adapters/TruSeq3-PE.fa:2:30:12:1:true SLIDINGWINDOW:4:20 MINLEN:16
```

rerun fastqc to control quality improvement.

## Mapping

Map your reads onto your genome with a dedicated mapper allowing splicing ex STAR, Hisat. BWA and Bowtie do not allow splicing and are used for full mapping (genome, chip, ...)

genologin:
```
module load bioinfo/STAR-2.7.9a
# step 1: genome index
STAR --runThreadN 1 --runMode genomeGenerate --genomeDir index --genomeFastaFiles JN3_SC00.fasta --sjdbGTFfile JN3_SC00.transcripts.gff --sjdbGTFtagExonParentTranscript Parent --sjdbGTFfeatureExon CDS --genomeSAindexNbases 9
# step 2: mapping
STAR --runThreadN 1 --genomeDir index --readFilesCommand zcat --readFilesIn 2022-242_S6.lepto.SC00.R1.paired.fastq.gz 2022-242_S6.lepto.SC00.R2.paired.fastq.gz --alignIntronMin 10 --alignIntronMax 5000 --alignMatesGapMax 5000 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix 2022-242_S6 --outFilterType BySJout --outWigType wiggle --outWigNorm None
```

Bam format explanation: 
[bioinformatics format file](https://sps-bioinfo.pages.mia.inra.fr/genome-annotation-workshop/pdf/GuideFormat.pdf)

[explain samflag](https://broadinstitute.github.io/picard/explain-flags.html)

genologin, explore bam file
```
module load bioinfo/samtools-1.16.1
samtools view 2022-242_S6Aligned.sortedByCoord.out.bam | head -n 4
samtools view -H 2022-242_S6Aligned.sortedByCoord.out.bam 
```
To validate mapping, read log file: 2022-242_S6Log.final.out

## Visualization

Download/Install [IGV](https://software.broadinstitute.org/software/igv/)

genologin, index bam file:
```
module load bioinfo/samtools-1.16.1
# index for fast querying / visualization
samtools index 2022-242_S6Aligned.sortedByCoord.out.bam
```

genologin, convert wigle to bigWig:
```
module load bioinfo/kentUtils-302.1.0
module load bioinfo/samtools-1.16.1
# get chromosome size
samtools faidx JN3_SC00.fasta
cut -f1,2 JN3_SC00.fasta.fai > JN3_SC00.chr.size
# convert wig to bigwig
wigToBigWig 2022-242_S6Signal.Unique.str1.out.wig JN3_SC00.chr.size 2022-242_S6Signal.Unique.str1.out.bw
wigToBigWig 2022-242_S6Signal.Unique.str2.out.wig JN3_SC00.chr.size 2022-242_S6Signal.Unique.str2.out.bw
``` 

transfer from genologin to your laptop:
* JN3_SC00.fasta (genome)
* JN3_SC00.transcripts.gff (annotations)
* 2022-242_S6Aligned.sortedByCoord.out.bam
* 2022-242_S6Aligned.sortedByCoord.out.bam.bai
* 2022-242_S6Signal.UniqueMultiple.str1.out.bw
* 2022-242_S6Signal.Unique.str2.out.bw

## Counts

Look at parameters to specify strand-specificity, multi-mapping, feature to count, ....

```
#Load environment
module load bioinfo/subread-1.6.0
featureCounts -a JN3_SC00.transcripts.gff -p -B -C -T 1 -s 2 -g Parent -t CDS -o 2022-242_S6.counts 2022-242_S6Aligned.sortedByCoord.out.bam
```
Look at 2022-242_S6.counts.summary for info on read counts.

## Summary / overview

write a report from all analyses:
```
module load bioinfo/MultiQC-v1.11
multiqc analysis_directory
```

